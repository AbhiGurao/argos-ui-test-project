# Argos Take-home project

Fix and improve the user experience and user interface of the Search page.

## Getting Started
To complete this task, you'll need:
  * Node : [Nodejs Website](https://nodejs.org/en/) Ideally v8.4.0 or above

* We've added an `.nvmrc` file to help with versions if you have problems. [Node version manager](https://github.com/creationix/nvm)

To install all the Nodejs packages, which are required, you'll need to run:
```
  npm install
```

In the root of the project directory.


## Running the project:
Running the project is simple, all you need to do is run - 

  ```
    npm run startAll
  ```

  That should bundle the project and start the server.

  * Client:
  http://localhost:8080/ 
  
  * Server: 
  http://localhost:8080/api/data 