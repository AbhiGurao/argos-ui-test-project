import React from 'react';
import {Container, Row, Col, Button, ButtonGroup, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import {GridView} from './gridView';
import {ListView} from './listView';
const request = require('superagent')
const payload = require('./payload.JSON')
var _ = require('lodash');


class SearchPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      keyword: "",
      allProductArr: [],
      searchList: [],
      viewType: "grid",
      searchType: "Price"
    };
    this.updateInputValue = this.updateInputValue.bind(this);
    this.changeListingView = this.changeListingView.bind(this);
    this.changeSearchType = this.changeSearchType.bind(this);
  }
  changeListingView(viewType) {
    this.setState({
      viewType: viewType
    });

  }
  changeSearchType(type){
    this.setState({
      searchType: type,
      keyword: "",
    }, () => this.updateInputValue(this.state.keyword));
  }

  updateInputValue(val) {
    this.setState({
      keyword: val
    });

    this.state.searchList = this.state.allProductArr.filter(v => {
      if(this.state.searchType != "Star Rating") {
        var searchObj = (this.state.searchType == "FastTrack") ? 'fastTrack' : 'price' 
        return (v.attributes[searchObj]).toString().includes(val)
      } else {
        return _.ceil((v.attributes.avgRating)).toString().includes(val)
      }
    });
  }

  componentDidMount() {
    var data = payload
    request.get('http://localhost:8080/api/data', (err, res) =>{
      //could get the json from the server instead ?
    });
    this.setState({
      allProductArr: data
    });
  }

  render() {
    return (
    <div>
      <Container className="mb-3">
        <Row>
          <Col><h1> Argos Search Page </h1></Col>
        </Row>
        <Row>
          <Col className="col-lg-12">
            <InputGroup className="mb-3">
              <DropdownButton
                as={InputGroup.Prepend}
                variant="outline-secondary"
                title={this.state.searchType}
                id="input-group-dropdown-1"
              >
                <Dropdown.Item onClick={() =>this.changeSearchType("Price")}>Price</Dropdown.Item>
                <Dropdown.Item onClick={() =>this.changeSearchType("Star Rating")}>Star Rating</Dropdown.Item>
                <Dropdown.Item onClick={() =>this.changeSearchType("FastTrack")}>FastTrack</Dropdown.Item>
              </DropdownButton>
              <FormControl
                placeholder="Filter by Price, Star Rating and FastTrack.."
                aria-label="Filter by Price, Star Rating and FastTrack.."
                aria-describedby="basic-search"
                onChange={(evt) =>this.updateInputValue(evt.target.value)}
                value={this.state.keyword}
              />
            </InputGroup>
          </Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col className="col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <ButtonGroup aria-label="Basic example">
              <Button variant="secondary" onClick={() => this.changeListingView("grid")}>Grid</Button>
              <Button variant="secondary" onClick={() => this.changeListingView("list")}>List</Button>
            </ButtonGroup>
          </Col>
        </Row>
      </Container>
      <div className="cardsHolder">
        {
          (this.state.viewType == "grid") ? 
            <GridView itemList={((this.state.searchList.length > 0) ? this.state.searchList : this.state.allProductArr)} /> :
            <ListView itemList={((this.state.searchList.length > 0) ? this.state.searchList : this.state.allProductArr)} />
        }
      </div>
    </div>
    )
  }
}

export default SearchPage
