import React from 'react';
import {Container, Row, Card, Button} from 'react-bootstrap';

function generategridView(data, i) {
    let productList = [];
    for(var count = i;count < (i+3) ; count++){
      if (data[count] == undefined) {
        break;
      } 
      var currentProduct = data[count];
      var details = currentProduct;
      productList.push(
        <div className="col-md-4 col-lg-4 col-xs-4 col-sm-4" key={"grid"+count}>
          <Card>
            <Card.Img className="cardImage" variant="top" src={'http://media.4rgos.it/s/Argos/'+ details.id + '_R_SET%3Fw=220&h=220'} />
            <Card.Body>
              <Card.Title>{details.attributes.name}</Card.Title>
              <Card.Text>
                Price: {details.attributes.price} StarRatings: {_.ceil(details.attributes.avgRating)}
              </Card.Text>
              <Card.Text>
                Reviews: {_.ceil(details.attributes.reviewsCount)}
              </Card.Text>
              <Card.Text>
              <a href={"http://www.argos.co.uk/product/"+ details.id} target="_blank"> More Info abut the product!</a>
              </Card.Text>
              <Button variant="primary">Add to trolley</Button>
            </Card.Body>
          </Card>
        </div>
      )
    }
    return productList
  }

export const GridView = ({itemList}) => {
    return itemList.map((e, i) => {
        if(i % 3 == 0){
            return (
                <Container key={i}>
                    <Row className="mb-1">
                        {
                            generategridView(itemList, i)
                        }
                    </Row>
                </Container>
            )
        }
    });
}