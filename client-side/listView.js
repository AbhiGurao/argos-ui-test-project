import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';

export const ListView = ({itemList}) => {
    return itemList.map((e, i) => {
        var currentProduct = e;
        var details = currentProduct
        {
            return(
                <Container key={"list"+i}>
                    <Row>
                        <Col className="col-lg-4">
                            <img style={{height: '200px', width:'200px'}} src={'http://media.4rgos.it/s/Argos/'+ details.id + '_R_SET%3Fw=220&h=220'} />
                        </Col>
                        <Col className="col-lg-8">
                            <p>
                                Title {details.attributes.name}
                            </p>
                            <p>
                                Price: {details.attributes.price}
                            </p>
                            <p>
                                StarRatings: {_.ceil(details.attributes.avgRating)}
                            </p>
                            <p>
                                Reviews: {_.ceil(details.attributes.reviewsCount)}
                            </p>
                                <a href={"http://www.argos.co.uk/product/"+ details.id} target="_blank"> More Info abut the product! </a>
                            <p>
                                <Button variant="primary">Add to trolley</Button>
                            </p>
                        </Col>
                    </Row>
                    <hr />
                </Container>
            )
        }
        
    })
}